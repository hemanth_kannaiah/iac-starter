output "public_ip" {
    value = aws_instance.SampServer.public_ip
    description = "Public IP address of the server"
}