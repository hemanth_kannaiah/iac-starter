provider "aws"{
    region = "eu-central-1"
}

resource "aws_launch_configuration" "sample_cluster" {
    image_id = "ami-03d15d623118d985c"
    instance_type = "t2.micro"
    security_groups = [ aws_security_group.instance.id ]
    user_data = <<-EOF
                #!/bin/bash
                echo "Hello, World" > index.html
                nohup busybox httpd -f -p ${var.server_port} &
                EOF

    # Required when using a launch configuration with an auto scaling group.
    # https://www.terraform.io/docs/providers/aws/r/launch_configuration.html
    lifecycle {
        create_before_destroy = true
    }
    
}

resource "aws_security_group" "instance" {
  name = "terraform-sample-instance"
  vpc_id = data.aws_vpc.default.id

  ingress {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = "Allow all http request"
    from_port = var.server_port
    protocol = "tcp"
    to_port = var.server_port
  } 
}

resource "aws_autoscaling_group" "sample" {
    launch_configuration = aws_launch_configuration.sample_cluster.name
    vpc_zone_identifier = data.aws_subnet_ids.default.ids

    target_group_arns = [ aws_lb_target_group.asg.arn ]
    health_check_type = "ELB"

    min_size = 2
    max_size = 10
    
    tag {
        key = "Name"
        value = "terraform-asg-sample"
        propagate_at_launch = true
    }
}

data "aws_vpc" "default" {
    default = true
}
data "aws_subnet_ids" "default" {
    vpc_id = data.aws_vpc.default.id
  
}

resource "aws_lb" "sample" {
    name = "terraform-asg-sample"
    load_balancer_type = "application"
    subnets = data.aws_subnet_ids.default.ids
    security_groups = [ aws_security_group.alb_grp.id ]
}

resource "aws_lb_listener" "http" {
    load_balancer_arn = aws_lb.sample.arn
    port = 80
    protocol = "HTTP"

    # By default, return a simple 404 page
    default_action {
        type = "fixed-response"
        fixed_response {
          content_type = "text/plain"
          message_body = "404: page not found"
          status_code = 404  
        }      
    }
}

resource "aws_lb_target_group" "asg" {
    name = "terraform-asg-sample"
    port = var.server_port
    protocol = "HTTP"
    vpc_id = data.aws_vpc.default.id

    health_check {
      path = "/"
      protocol = "HTTP"
      matcher = "200"
      interval = 15
      timeout = 3
      healthy_threshold = 2
      unhealthy_threshold = 2
    }
}

resource "aws_security_group" "alb_grp" {
    name = "terraform-sample-alb"

    # allow inbound HTTP requests 
    ingress {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 80
      to_port = 80
      protocol = "tcp"
    }

    # allow all outboud traffic
    egress {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 0
      to_port = 0
      protocol = "-1"
    } 
}

resource "aws_lb_listener_rule" "asg" {
    listener_arn = aws_lb_listener.http.arn
    priority = 100
    condition {
      path_pattern{
          values = ["*"]
      } 
    }
    action {
      type = "forward"
      target_group_arn = aws_lb_target_group.asg.arn
    }
}